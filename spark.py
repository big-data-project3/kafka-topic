import os

from pyspark.sql import SparkSession
from pyspark.sql.functions import col, expr, from_json
from pyspark.sql.types import DoubleType, StringType, StructField, StructType 


os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-streaming-kafka-0-10_2.12:3.2.0,org.apache.spark:spark-sql-kafka-0-10_2.12:3.2.0 pyspark-shell'


KAFKA_TOPIC = "properties"
KAFKA_BOOTSTRAP_SERVER = "localhost:9092"

schema = (
    StructType()
    .add("hits", StructType([
        StructField("hits", StructType([
            StructField("_id", StringType()),
            StructField("_source", StructType([
                StructField("listing", StructType([
                    StructField("area", DoubleType()),
                    StructField("price", DoubleType())
                ]))
            ]))
        ]))
    ]))
)

spark = (SparkSession.builder.appName("Kafka Pyspark Streaming")
        .master("local[*]")
        .config("spark.cassandra.connection.host", "localhost")
        .config("spark.cassandra.connection.port", "9042")
        .getOrCreate()
)


input_df = (
    spark.readStream.format("kafka")
    .option("kafka.bootstrap.servers", KAFKA_BOOTSTRAP_SERVER)
    .option("subscribe", KAFKA_TOPIC)
    .option("startingOffsets", "earliest")
    .load()
)

value_df = input_df.select(from_json(col("value").cast("string"),schema).alias("value"))

exploded_df = value_df.selectExpr("value.hits.hits._id",
                                  "value.hits.hits._source.listing.area",
                                  "value.hits.hits._source.listing.price").alias("exploded")

exploded_df.printSchema()

output_query = (
    exploded_df
    .writeStream
    .format("csv")
    .option("path", "./data")
    .option("checkpointLocation", "chck-pnt-dir-kh")
    .outputMode("append")
    .start()
)

output_query.awaitTermination()
